import sys
import os
import glob
import numpy as np
import re

from readTrc import *

# take 1 file and output number of pulses, and ToT.

def count_pulses( t_samples, v_samples, th_level=.22):
    total_num_pulses = 0
    th_flag= True
    for vv in v_samples:
        if ( vv>th_level)  and th_flag:
            th_flag = False
            total_num_pulses +=1
        elif ( vv <= th_level):
            th_flag = True
    return total_num_pulses

"""
680 --> 1 sample per config
460 --> 1 sample (avoid 00)
330 --> 2 samples
220 --> 2 samples
150 --> 2 samples
100 --> 2 samples
68 --> 2 samples
until 10 2 samples, then 3 samples per config
Info about channels

Layer 3 pFEB Board 1040 --> Board 5 --> VMM B (1)--> 36 (pi =300) --> CH2 scope
Layer 4 pFeb Board 993 --> Board 4 --> VMM C(2)  --> 27 (pi =100)--> CH1
Layer 3 pFEB Board 1040 --> Board 5 --> VMM C (2)--> 10 (pi =100)--> CH3 scope
Layer 4 pFeb Board 993 --> Board 4 --> VMM B(1)  --> 53 (pi =470)--> CH4 scope



"""
"""
run_config = {1: {'C1': 'L4 VMM2 ch 3  100pF','C2': 'L3 VMM1 ch 60 300pF','C3': 'L3 VMM2 ch 20 100pF','C4': 'L4 VMM1 ch 33 200pF'},
              2: {'C1': 'L4 VMM2 ch 15 100pF','C2': 'L3 VMM1 ch 43 150pF','C3': 'L3 VMM2 ch 26 100pF','C4': 'L4 VMM1 ch 37 470pF'}}
"""
#vmm channel
run_config = {1: {'C1':3,'C2': 60,'C3':20,'C4':33},
              2: {'C1':15 ,'C2':43,'C3':26,'C4':37}}

files_per_run = { '680':1,'460':1,
                  '330':2,'220':2,
                  '150':2,'100':2,
                  '68':2,'46':2,'33':2,
                  '22':2,'15':2,'10':2,
                  '6.8':3,'4.6':3,'3.3':3,
                  '2.2':3,'1.5':3,'1':3}
th_per_pad = {1: {'C1':.220,'C2':.220,'C3':.220,'C4':.220},
              2: {'C1':.220,'C2':.300,'C3':.220,'C4':.220}}

def parse_data(file_path):
    attenuation_filter = re.search('(?<=./)[\w\s.]+',file_path)
    sample_number = re.search('(?<=[0-9]{4})\w',file_path)
    scope_channel = re.search('[C][0-9]',file_path)
    return attenuation_filter.group(), scope_channel.group(), sample_number.group()

fout = open('rates.dat','w')
print "sample\t att\t scope_ch\t vmm_ch \t rate" 
for fileName in glob.glob('./*/*.trc'):
    #print fileName
    att, scope_ch, sample_num = parse_data(fileName)
    sample_num = int(sample_num)
    #print  att, scope_ch, sample_num
    samples_per_run = files_per_run[str(att)]
    
    if (samples_per_run == 1):
        if(sample_num == 0):
            th_level = th_per_pad[1][scope_ch]
            vmm_ch = run_config[1][scope_ch]
        else:
            th_level = th_per_pad[2][scope_ch]
            vmm_ch = run_config[2][scope_ch]
    elif(samples_per_run ==2):
        if(sample_num <2):
            th_level = th_per_pad[1][scope_ch]
            vmm_ch = run_config[1][scope_ch]
        else:
            th_level = th_per_pad[2][scope_ch]
            vmm_ch = run_config[2][scope_ch]
    elif (samples_per_run==3):
        if(sample_num <3):
            th_level = th_per_pad[1][scope_ch]
            vmm_ch = run_config[1][scope_ch]
        else:
            th_level = th_per_pad[2][scope_ch]
            vmm_ch = run_config[2][scope_ch]
    
    tt,vv,meta = readTrc(fileName)
    pulses=count_pulses(tt,vv,th_level)
    rate = pulses/1.6
    out="%d\t%f\t%s\t%d\t%f"%(sample_num, float(att), scope_ch, vmm_ch, float(rate))
    print out
    fout.write(out)

